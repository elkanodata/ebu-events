# EBU Events

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8899
$ npm dev

# build for elkano
$ npm build
```